﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Ais_school.Model
{
    class DbConnector
    {
        private MySqlCommand _command;
        private MySqlConnection _connection;
        private static DbConnector _instance = null;

        private DbConnector()
        {
            string connectionString = "Server=localhost;Port=3306;User=root;Password=1234;Database=ais_school";
            _connection = new MySqlConnection(connectionString);
            _connection.Open();

            _command = new MySqlCommand();
            _command.Connection = _connection;
        }

        public static DbConnector GetInstance()
        {
            if(_instance == null)
            {
                _instance = new DbConnector();
            }
            return _instance;
        }

        public MySqlCommand GetMySqlCommand()
        {
            return _command;
        }
    }
}
