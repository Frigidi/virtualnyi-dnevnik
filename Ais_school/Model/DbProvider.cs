﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ais_school.Model
{
    class DbProvider
    {
        private static DbProvider instance = null;
        public TableSubjects tableSubject { get; private set; }
        public TableStudents tableStudents { get; private set; }
        public TableMarks tableMarks { get; private set; }

        private DbProvider()
        {
            tableStudents = new TableStudents();
            tableSubject = new TableSubjects();
            tableMarks = new TableMarks(tableStudents,tableSubject);
        }

        public static DbProvider GetInstance()
        {
            if(instance == null)
            {
                instance = new DbProvider();
            }
            return instance;
        }
    }
}
