﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.ComponentModel;

namespace Ais_school.Model
{
    class TableSubjects
    {
        private MySqlCommand _command;
        public BindingList<Subject> Rows { get; private set; }

        public TableSubjects()
        {
            _command = DbConnector.GetInstance().GetMySqlCommand();
            LoadRows();
        }

        private void LoadRows()
        {
            Rows = new BindingList<Subject>();
            _command.CommandText = "CALL subjects_select_all()";

            MySqlDataReader dataReader = _command.ExecuteReader();

            while (dataReader.Read())
            {
                Rows.Add(new Subject()
                {
                    Id = dataReader.GetInt32("id"),
                    Name = dataReader.GetString("name")
                });
            }
            dataReader.Close();
        }
    }
}
