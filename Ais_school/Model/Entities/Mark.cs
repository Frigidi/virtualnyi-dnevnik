﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ais_school.Model
{
    class Mark
    {
        public int Id { get; set; }
        public DateTime Datetime { get; set; }
        public Student Student { get; set; }
        public Subject Subject { get; set; }
        public int Grade { get; set; }
    }
}
