﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ais_school.Model
{
    class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Group { get; set; }
        public int Age { get; set; }

        public override string ToString()
        {
            return Name ; //+" " + Group
        }
    }
}
