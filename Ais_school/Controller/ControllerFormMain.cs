﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Ais_school.Model;

namespace Ais_school.Controller
{
    class ControllerFormMain
    {
        private DbProvider dbProvider;
        private FormMain formMain;

        public ControllerFormMain(FormMain fM)
        {
            formMain = fM;
            try
            {
                dbProvider = DbProvider.GetInstance();
            }
            catch
            {
                Application.Exit();
            }
        }

        #region TableSudents
        public void FillDGVStudents()
        {
            formMain.dataGridViewStudents.DataSource = dbProvider.tableStudents.Rows;
            formMain.dataGridViewStudents.Columns["Id"].Visible = false;
        }

        public void FillSelectStudentFields()
        {
            if (formMain.dataGridViewStudents.SelectedRows.Count != 0)
            {
                DataGridViewRow row = formMain.dataGridViewStudents.SelectedRows[0];
                formMain.textBoxName.Text = row.Cells["Name"].Value.ToString();
                formMain.numericUpDownAge.Value = decimal.Parse(row.Cells["Age"].Value.ToString());
                formMain.textBoxClass.Text = row.Cells["Group"].Value.ToString();
                formMain.textBoxId.Text = row.Cells["Id"].Value.ToString();
            }
        }

        public void ClearStudentsFields()
        {
            formMain.textBoxName.Clear();
            formMain.numericUpDownAge.Value = 7;
            formMain.textBoxClass.Clear();
            formMain.textBoxId.Clear();
        }

        public void AddStudent()
        {
            string name = formMain.textBoxName.Text;
            int age = (int)formMain.numericUpDownAge.Value;
            string group = formMain.textBoxClass.Text;

            if (name == string.Empty || group == string.Empty)
            {
                MessageBox.Show("Заполните поля ввода информации");
            }
            else
            {
                Student student = new Student()
                {
                    Name = name,
                    Age = age,
                    Group = group
                };

                if (dbProvider.tableStudents.Add(student))
                {
                    MessageBox.Show("Студент успешно добавлен!");
                    ClearStudentsFields();

                }
                else
                {
                    MessageBox.Show("Ошибка при добавлении!");
                }
            }
        }

        public void DeleteStudent()
        {
            string id = formMain.textBoxId.Text;
            if (id == string.Empty)
            {
                MessageBox.Show("Выберите строку для удаления");
            }
            else
            {
                if (dbProvider.tableStudents.Delete(int.Parse(id)))
                {
                    MessageBox.Show("Студент удален!");
                    ClearStudentsFields();
                }
                else
                {
                    MessageBox.Show("Не удалось удалить");
                }
            }
        }

        public void UpdateStudent()
        {
            string name = formMain.textBoxName.Text;
            int age = (int)formMain.numericUpDownAge.Value;
            string group = formMain.textBoxClass.Text;
            string id = formMain.textBoxId.Text;

            if (name == string.Empty || group == string.Empty || id == string.Empty)
            {
                MessageBox.Show("Заполните все поля!");
            }
            else
            {
                Student student = new Student()
                {
                    Id = int.Parse(id),
                    Name = name,
                    Age = age,
                    Group = group
                };

                if (dbProvider.tableStudents.Update(student))
                {
                    MessageBox.Show("Студент успешно обновлен!");
                    ClearStudentsFields();
                    formMain.dataGridViewStudents.Refresh();
                    formMain.comboBoxMarkStudent.Refresh();
                }
                else
                {
                    MessageBox.Show("Ошибка при обновлении");
                }
            }
        }
        #endregion

        #region TableMarks
        public void FillDGVMarks()
        {
            formMain.dataGridViewMarks.DataSource = dbProvider.tableMarks.Rows;
            formMain.dataGridViewMarks.Columns["Id"].Visible = false;

            formMain.dataGridViewMarks.Columns["DateTime"].DefaultCellStyle.Format = "dd.MM.yyyy H:mm:ss";
        }

        public void FillSelectMarkField()
        {
            if (formMain.dataGridViewMarks.SelectedRows.Count != 0)
            {
                DataGridViewRow row = formMain.dataGridViewMarks.SelectedRows[0];
                formMain.dateTimePickerMarksDatetime.Value = DateTime.Parse(row.Cells["Datetime"].Value.ToString());

                for (int i = 0; i < formMain.comboBoxMarkStudent.Items.Count; i++)
                {
                    if (formMain.comboBoxMarkStudent.Items[i].ToString() == row.Cells["Student"].Value.ToString())
                    {
                        formMain.comboBoxMarkStudent.SelectedIndex = i;
                    }
                }

                for (int i = 0; i < formMain.comboBoxMarksSubjects.Items.Count; i++)
                {
                    if (formMain.comboBoxMarksSubjects.Items[i].ToString() == row.Cells["Subject"].Value.ToString())
                    {
                        formMain.comboBoxMarksSubjects.SelectedIndex = i;
                    }
                }

                formMain.numericUpDownMarkGrade.Value = decimal.Parse(row.Cells["Grade"].Value.ToString());
                formMain.textBoxMarksId.Text = row.Cells["Id"].Value.ToString();
            }
        }

        public void FillComboboxStudents()
        {
            formMain.comboBoxMarkStudent.DataSource = null;
            formMain.comboBoxSearchStudent.DataSource = null;

            formMain.comboBoxMarkStudent.DataSource = dbProvider.tableStudents.Rows;
            formMain.comboBoxSearchStudent.DataSource = dbProvider.tableStudents.Rows;
        }

        public void FillComboboxSubjects()
        {
            formMain.comboBoxMarksSubjects.DataSource = dbProvider.tableSubject.Rows;
            formMain.comboBoxSearchSubject.DataSource = dbProvider.tableSubject.Rows;
        }

        public void AddMark()
        {
            DateTime dateTime = formMain.dateTimePickerMarksDatetime.Value;
            Student student = (Student)formMain.comboBoxMarkStudent.SelectedItem;
            Subject subject = (Subject)formMain.comboBoxMarksSubjects.SelectedItem;
            int grade = (int)formMain.numericUpDownMarkGrade.Value;

            Mark mark = new Mark()
            {
                Datetime = dateTime,
                Student = student,
                Subject = subject,
                Grade = grade
            };

            if (dbProvider.tableMarks.Add(mark))
            {
                MessageBox.Show("Оценка успешно добавлена!");
            }
            else
            {
                MessageBox.Show("Ошибка при добавлении");
            }
        }

        public void DeleteMark()
        {
            string id = formMain.textBoxMarksId.Text;

            if (id == string.Empty)
            {
                MessageBox.Show("Не выбрана стока для удаления");
            }
            else
            {
                if (dbProvider.tableMarks.Delete(int.Parse(id)))
                {
                    MessageBox.Show("Оценка была удалена");
                    formMain.textBoxMarksId.Clear();
                }
                else
                {
                    MessageBox.Show("Ошибка при удалении");
                }
            }
        }

        public void UpdateMark()
        {
            string id = formMain.textBoxMarksId.Text;
            DateTime dateTime = formMain.dateTimePickerMarksDatetime.Value;
            Student student = (Student)formMain.comboBoxMarkStudent.SelectedItem;
            Subject subject = (Subject)formMain.comboBoxMarksSubjects.SelectedItem;
            int grade = (int)formMain.numericUpDownMarkGrade.Value;

            if (id == string.Empty)
            {
                MessageBox.Show("Не выбрана строка для обновления");
            }
            else
            {
                Mark mark = new Mark()
                {
                    Id = int.Parse(id),
                    Datetime = dateTime,
                    Student = student,
                    Subject = subject,
                    Grade = grade
                };

                if (dbProvider.tableMarks.Update(mark))
                {
                    MessageBox.Show("Оценка была обновлена");
                    formMain.textBoxMarksId.Clear();
                    formMain.dataGridViewMarks.Refresh();
                }
                else
                {
                    MessageBox.Show("Ошибка при обновлении");
                }
            }
        }
        #endregion

        #region Поиск

        public void ResetSearsh()
        {
            formMain.dataGridViewSearch.DataSource = dbProvider.tableMarks.Rows;
            formMain.dataGridViewSearch.Columns["Id"].Visible = false;

            formMain.dataGridViewSearch.Columns["DateTime"].DefaultCellStyle.Format = "dd.MM.yyyy H:mm:ss";

            formMain.checkBoxDateSearch.Checked = false;
            formMain.checkBoxStudetnSearch.Checked = false;
            formMain.checkBoxSubjectSearch.Checked = false;
}

        public void Search()
        {
            List<Mark> findMarks = new List<Mark>();

            DateTime start = formMain.dateTimePickerSearchDatetimeStart.Value;
            DateTime finish = formMain.dateTimePickerSearchDatetimeFinish.Value;

            int idStudent = ((Student)formMain.comboBoxSearchStudent.SelectedItem).Id;
            int idSubject = ((Subject)formMain.comboBoxSearchSubject.SelectedItem).Id;

            for (int i = 0; i < dbProvider.tableMarks.Rows.Count; i++)
            {
                if(formMain.checkBoxDateSearch.Checked)
                {
                    if(dbProvider.tableMarks.Rows[i].Datetime<start || dbProvider.tableMarks.Rows[i].Datetime>finish)
                    {
                        continue;
                    }
                }

                if (formMain.checkBoxStudetnSearch.Checked)
                {
                    if (dbProvider.tableMarks.Rows[i].Student.Id!=idStudent)
                    {
                        continue;
                    }
                }

                if (formMain.checkBoxSubjectSearch.Checked)
                {
                    if (dbProvider.tableMarks.Rows[i].Subject.Id != idSubject)
                    {
                        continue;
                    }
                }


                findMarks.Add(dbProvider.tableMarks.Rows[i]);
            }


            formMain.dataGridViewSearch.DataSource = findMarks;
            formMain.dataGridViewSearch.Columns["Id"].Visible = false;

            formMain.dataGridViewSearch.Columns["DateTime"].DefaultCellStyle.Format = "dd.MM.yyyy H:mm:ss";
        }
        #endregion
    }
}
