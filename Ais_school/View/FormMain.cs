﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Ais_school.Controller;

namespace Ais_school
{
    public partial class FormMain : Form
    {
        private ControllerFormMain controler;

        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            controler = new ControllerFormMain(this);
            controler.FillDGVStudents();
            controler.FillDGVMarks();
            controler.FillComboboxStudents();
            controler.FillComboboxSubjects();
            controler.ResetSearsh();
        }

        private void buttonStudentAdd_Click(object sender, EventArgs e)
        {
            controler.AddStudent();
        }

        private void dataGridViewStudents_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            controler.FillSelectStudentFields();
        }

        private void buttonStudentDelete_Click(object sender, EventArgs e)
        {
            controler.DeleteStudent();
        }

        private void buttonStudentUpdate_Click(object sender, EventArgs e)
        {
            controler.UpdateStudent();
            controler.FillComboboxStudents();
        }

        private void buttonMarkAdd_Click(object sender, EventArgs e)
        {
            controler.AddMark();
        }

        private void buttonMarkDelete_Click(object sender, EventArgs e)
        {
            controler.DeleteMark();
        }

        private void dataGridViewMarks_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            controler.FillSelectMarkField();
        }

        private void buttonMarkUpdate_Click(object sender, EventArgs e)
        {
            controler.UpdateMark();
        }

        private void buttonMarksSearch_Click(object sender, EventArgs e)
        {
            controler.Search();
        }

        private void buttonSearchReset_Click(object sender, EventArgs e)
        {
            controler.ResetSearsh();
        }
    }
}
