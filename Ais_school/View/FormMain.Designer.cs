﻿namespace Ais_school
{
    partial class FormMain
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBoxId = new System.Windows.Forms.TextBox();
            this.buttonStudentDelete = new System.Windows.Forms.Button();
            this.buttonStudentUpdate = new System.Windows.Forms.Button();
            this.buttonStudentAdd = new System.Windows.Forms.Button();
            this.numericUpDownAge = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxClass = new System.Windows.Forms.TextBox();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.dataGridViewStudents = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonMarkUpdate = new System.Windows.Forms.Button();
            this.buttonMarkDelete = new System.Windows.Forms.Button();
            this.buttonMarkAdd = new System.Windows.Forms.Button();
            this.textBoxMarksId = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxMarksSubjects = new System.Windows.Forms.ComboBox();
            this.numericUpDownMarkGrade = new System.Windows.Forms.NumericUpDown();
            this.comboBoxMarkStudent = new System.Windows.Forms.ComboBox();
            this.dateTimePickerMarksDatetime = new System.Windows.Forms.DateTimePicker();
            this.dataGridViewMarks = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.dateTimePickerSearchDatetimeFinish = new System.Windows.Forms.DateTimePicker();
            this.buttonMarksSearch = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBoxSearchSubject = new System.Windows.Forms.ComboBox();
            this.comboBoxSearchStudent = new System.Windows.Forms.ComboBox();
            this.dateTimePickerSearchDatetimeStart = new System.Windows.Forms.DateTimePicker();
            this.dataGridViewSearch = new System.Windows.Forms.DataGridView();
            this.checkBoxDateSearch = new System.Windows.Forms.CheckBox();
            this.checkBoxStudetnSearch = new System.Windows.Forms.CheckBox();
            this.checkBoxSubjectSearch = new System.Windows.Forms.CheckBox();
            this.buttonSearchReset = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStudents)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMarkGrade)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMarks)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSearch)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(996, 440);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.dataGridViewStudents);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage1.Size = new System.Drawing.Size(988, 411);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Ученики";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxId);
            this.groupBox1.Controls.Add(this.buttonStudentDelete);
            this.groupBox1.Controls.Add(this.buttonStudentUpdate);
            this.groupBox1.Controls.Add(this.buttonStudentAdd);
            this.groupBox1.Controls.Add(this.numericUpDownAge);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBoxClass);
            this.groupBox1.Controls.Add(this.textBoxName);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(4, 175);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(980, 203);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Работа со студентами";
            // 
            // textBoxId
            // 
            this.textBoxId.Location = new System.Drawing.Point(825, 48);
            this.textBoxId.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxId.Name = "textBoxId";
            this.textBoxId.Size = new System.Drawing.Size(132, 22);
            this.textBoxId.TabIndex = 14;
            this.textBoxId.Visible = false;
            // 
            // buttonStudentDelete
            // 
            this.buttonStudentDelete.Location = new System.Drawing.Point(369, 123);
            this.buttonStudentDelete.Margin = new System.Windows.Forms.Padding(4);
            this.buttonStudentDelete.Name = "buttonStudentDelete";
            this.buttonStudentDelete.Size = new System.Drawing.Size(100, 28);
            this.buttonStudentDelete.TabIndex = 13;
            this.buttonStudentDelete.Text = "Удалить";
            this.buttonStudentDelete.UseVisualStyleBackColor = true;
            this.buttonStudentDelete.Click += new System.EventHandler(this.buttonStudentDelete_Click);
            // 
            // buttonStudentUpdate
            // 
            this.buttonStudentUpdate.Location = new System.Drawing.Point(195, 123);
            this.buttonStudentUpdate.Margin = new System.Windows.Forms.Padding(4);
            this.buttonStudentUpdate.Name = "buttonStudentUpdate";
            this.buttonStudentUpdate.Size = new System.Drawing.Size(100, 28);
            this.buttonStudentUpdate.TabIndex = 12;
            this.buttonStudentUpdate.Text = "Обновить";
            this.buttonStudentUpdate.UseVisualStyleBackColor = true;
            this.buttonStudentUpdate.Click += new System.EventHandler(this.buttonStudentUpdate_Click);
            // 
            // buttonStudentAdd
            // 
            this.buttonStudentAdd.Location = new System.Drawing.Point(20, 123);
            this.buttonStudentAdd.Margin = new System.Windows.Forms.Padding(4);
            this.buttonStudentAdd.Name = "buttonStudentAdd";
            this.buttonStudentAdd.Size = new System.Drawing.Size(100, 28);
            this.buttonStudentAdd.TabIndex = 11;
            this.buttonStudentAdd.Text = "Добавить";
            this.buttonStudentAdd.UseVisualStyleBackColor = true;
            this.buttonStudentAdd.Click += new System.EventHandler(this.buttonStudentAdd_Click);
            // 
            // numericUpDownAge
            // 
            this.numericUpDownAge.Location = new System.Drawing.Point(195, 81);
            this.numericUpDownAge.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDownAge.Minimum = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.numericUpDownAge.Name = "numericUpDownAge";
            this.numericUpDownAge.Size = new System.Drawing.Size(160, 22);
            this.numericUpDownAge.TabIndex = 10;
            this.numericUpDownAge.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(383, 57);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "Класс";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(191, 57);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 17);
            this.label2.TabIndex = 8;
            this.label2.Text = "Возраст";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 57);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 17);
            this.label1.TabIndex = 7;
            this.label1.Text = "Имя";
            // 
            // textBoxClass
            // 
            this.textBoxClass.Location = new System.Drawing.Point(387, 80);
            this.textBoxClass.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxClass.Name = "textBoxClass";
            this.textBoxClass.Size = new System.Drawing.Size(132, 22);
            this.textBoxClass.TabIndex = 6;
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(16, 80);
            this.textBoxName.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(132, 22);
            this.textBoxName.TabIndex = 4;
            // 
            // dataGridViewStudents
            // 
            this.dataGridViewStudents.AllowUserToAddRows = false;
            this.dataGridViewStudents.AllowUserToDeleteRows = false;
            this.dataGridViewStudents.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewStudents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewStudents.Dock = System.Windows.Forms.DockStyle.Top;
            this.dataGridViewStudents.Location = new System.Drawing.Point(4, 4);
            this.dataGridViewStudents.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridViewStudents.MultiSelect = false;
            this.dataGridViewStudents.Name = "dataGridViewStudents";
            this.dataGridViewStudents.ReadOnly = true;
            this.dataGridViewStudents.Size = new System.Drawing.Size(980, 171);
            this.dataGridViewStudents.TabIndex = 0;
            this.dataGridViewStudents.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewStudents_CellClick);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Controls.Add(this.dataGridViewMarks);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage2.Size = new System.Drawing.Size(988, 411);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Электронный дневник";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buttonMarkUpdate);
            this.groupBox2.Controls.Add(this.buttonMarkDelete);
            this.groupBox2.Controls.Add(this.buttonMarkAdd);
            this.groupBox2.Controls.Add(this.textBoxMarksId);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.comboBoxMarksSubjects);
            this.groupBox2.Controls.Add(this.numericUpDownMarkGrade);
            this.groupBox2.Controls.Add(this.comboBoxMarkStudent);
            this.groupBox2.Controls.Add(this.dateTimePickerMarksDatetime);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(4, 175);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(980, 219);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Работа с дневником";
            // 
            // buttonMarkUpdate
            // 
            this.buttonMarkUpdate.Location = new System.Drawing.Point(458, 137);
            this.buttonMarkUpdate.Name = "buttonMarkUpdate";
            this.buttonMarkUpdate.Size = new System.Drawing.Size(185, 32);
            this.buttonMarkUpdate.TabIndex = 14;
            this.buttonMarkUpdate.Text = "Обновить оценку";
            this.buttonMarkUpdate.UseVisualStyleBackColor = true;
            this.buttonMarkUpdate.Click += new System.EventHandler(this.buttonMarkUpdate_Click);
            // 
            // buttonMarkDelete
            // 
            this.buttonMarkDelete.Location = new System.Drawing.Point(253, 137);
            this.buttonMarkDelete.Name = "buttonMarkDelete";
            this.buttonMarkDelete.Size = new System.Drawing.Size(185, 32);
            this.buttonMarkDelete.TabIndex = 13;
            this.buttonMarkDelete.Text = "Удалить оценку";
            this.buttonMarkDelete.UseVisualStyleBackColor = true;
            this.buttonMarkDelete.Click += new System.EventHandler(this.buttonMarkDelete_Click);
            // 
            // buttonMarkAdd
            // 
            this.buttonMarkAdd.Location = new System.Drawing.Point(17, 137);
            this.buttonMarkAdd.Name = "buttonMarkAdd";
            this.buttonMarkAdd.Size = new System.Drawing.Size(200, 32);
            this.buttonMarkAdd.TabIndex = 12;
            this.buttonMarkAdd.Text = "Выставить оценку";
            this.buttonMarkAdd.UseVisualStyleBackColor = true;
            this.buttonMarkAdd.Click += new System.EventHandler(this.buttonMarkAdd_Click);
            // 
            // textBoxMarksId
            // 
            this.textBoxMarksId.Location = new System.Drawing.Point(900, 21);
            this.textBoxMarksId.Name = "textBoxMarksId";
            this.textBoxMarksId.Size = new System.Drawing.Size(62, 22);
            this.textBoxMarksId.TabIndex = 11;
            this.textBoxMarksId.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(680, 41);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 17);
            this.label7.TabIndex = 10;
            this.label7.Text = "Оценка";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(455, 41);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 17);
            this.label6.TabIndex = 9;
            this.label6.Text = "Предмет";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(250, 41);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "Ученик";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "Дата и время";
            // 
            // comboBoxMarksSubjects
            // 
            this.comboBoxMarksSubjects.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMarksSubjects.FormattingEnabled = true;
            this.comboBoxMarksSubjects.Location = new System.Drawing.Point(458, 77);
            this.comboBoxMarksSubjects.Name = "comboBoxMarksSubjects";
            this.comboBoxMarksSubjects.Size = new System.Drawing.Size(185, 24);
            this.comboBoxMarksSubjects.TabIndex = 6;
            // 
            // numericUpDownMarkGrade
            // 
            this.numericUpDownMarkGrade.Location = new System.Drawing.Point(683, 79);
            this.numericUpDownMarkGrade.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericUpDownMarkGrade.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numericUpDownMarkGrade.Name = "numericUpDownMarkGrade";
            this.numericUpDownMarkGrade.Size = new System.Drawing.Size(120, 22);
            this.numericUpDownMarkGrade.TabIndex = 5;
            this.numericUpDownMarkGrade.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // comboBoxMarkStudent
            // 
            this.comboBoxMarkStudent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMarkStudent.FormattingEnabled = true;
            this.comboBoxMarkStudent.Location = new System.Drawing.Point(253, 77);
            this.comboBoxMarkStudent.Name = "comboBoxMarkStudent";
            this.comboBoxMarkStudent.Size = new System.Drawing.Size(185, 24);
            this.comboBoxMarkStudent.TabIndex = 3;
            // 
            // dateTimePickerMarksDatetime
            // 
            this.dateTimePickerMarksDatetime.CustomFormat = "yyyy-MM-dd H:mm:ss";
            this.dateTimePickerMarksDatetime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerMarksDatetime.Location = new System.Drawing.Point(17, 77);
            this.dateTimePickerMarksDatetime.Name = "dateTimePickerMarksDatetime";
            this.dateTimePickerMarksDatetime.Size = new System.Drawing.Size(200, 22);
            this.dateTimePickerMarksDatetime.TabIndex = 2;
            // 
            // dataGridViewMarks
            // 
            this.dataGridViewMarks.AllowUserToAddRows = false;
            this.dataGridViewMarks.AllowUserToDeleteRows = false;
            this.dataGridViewMarks.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewMarks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewMarks.Dock = System.Windows.Forms.DockStyle.Top;
            this.dataGridViewMarks.Location = new System.Drawing.Point(4, 4);
            this.dataGridViewMarks.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridViewMarks.MultiSelect = false;
            this.dataGridViewMarks.Name = "dataGridViewMarks";
            this.dataGridViewMarks.ReadOnly = true;
            this.dataGridViewMarks.Size = new System.Drawing.Size(980, 171);
            this.dataGridViewMarks.TabIndex = 1;
            this.dataGridViewMarks.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewMarks_CellClick);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox3);
            this.tabPage3.Controls.Add(this.dataGridViewSearch);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(988, 411);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Поиск";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.buttonSearchReset);
            this.groupBox3.Controls.Add(this.checkBoxSubjectSearch);
            this.groupBox3.Controls.Add(this.checkBoxStudetnSearch);
            this.groupBox3.Controls.Add(this.checkBoxDateSearch);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.dateTimePickerSearchDatetimeFinish);
            this.groupBox3.Controls.Add(this.buttonMarksSearch);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.comboBoxSearchSubject);
            this.groupBox3.Controls.Add(this.comboBoxSearchStudent);
            this.groupBox3.Controls.Add(this.dateTimePickerSearchDatetimeStart);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(3, 174);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(982, 231);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Параметры поиска";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(241, 47);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(27, 17);
            this.label11.TabIndex = 21;
            this.label11.Text = "До";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(20, 47);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(26, 17);
            this.label10.TabIndex = 20;
            this.label10.Text = "От";
            // 
            // dateTimePickerSearchDatetimeFinish
            // 
            this.dateTimePickerSearchDatetimeFinish.CustomFormat = "yyyy-MM-dd H:mm:ss";
            this.dateTimePickerSearchDatetimeFinish.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerSearchDatetimeFinish.Location = new System.Drawing.Point(244, 81);
            this.dateTimePickerSearchDatetimeFinish.Name = "dateTimePickerSearchDatetimeFinish";
            this.dateTimePickerSearchDatetimeFinish.Size = new System.Drawing.Size(200, 22);
            this.dateTimePickerSearchDatetimeFinish.TabIndex = 19;
            // 
            // buttonMarksSearch
            // 
            this.buttonMarksSearch.Location = new System.Drawing.Point(300, 164);
            this.buttonMarksSearch.Name = "buttonMarksSearch";
            this.buttonMarksSearch.Size = new System.Drawing.Size(200, 32);
            this.buttonMarksSearch.TabIndex = 16;
            this.buttonMarksSearch.Text = "Найти оценки";
            this.buttonMarksSearch.UseVisualStyleBackColor = true;
            this.buttonMarksSearch.Click += new System.EventHandler(this.buttonMarksSearch_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(778, 47);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 17);
            this.label8.TabIndex = 15;
            this.label8.Text = "Предмет";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(512, 47);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 17);
            this.label9.TabIndex = 14;
            this.label9.Text = "Ученик";
            // 
            // comboBoxSearchSubject
            // 
            this.comboBoxSearchSubject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSearchSubject.FormattingEnabled = true;
            this.comboBoxSearchSubject.Location = new System.Drawing.Point(781, 83);
            this.comboBoxSearchSubject.Name = "comboBoxSearchSubject";
            this.comboBoxSearchSubject.Size = new System.Drawing.Size(185, 24);
            this.comboBoxSearchSubject.TabIndex = 12;
            // 
            // comboBoxSearchStudent
            // 
            this.comboBoxSearchStudent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSearchStudent.FormattingEnabled = true;
            this.comboBoxSearchStudent.Location = new System.Drawing.Point(515, 83);
            this.comboBoxSearchStudent.Name = "comboBoxSearchStudent";
            this.comboBoxSearchStudent.Size = new System.Drawing.Size(185, 24);
            this.comboBoxSearchStudent.TabIndex = 11;
            // 
            // dateTimePickerSearchDatetimeStart
            // 
            this.dateTimePickerSearchDatetimeStart.CustomFormat = "yyyy-MM-dd H:mm:ss";
            this.dateTimePickerSearchDatetimeStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerSearchDatetimeStart.Location = new System.Drawing.Point(20, 81);
            this.dateTimePickerSearchDatetimeStart.Name = "dateTimePickerSearchDatetimeStart";
            this.dateTimePickerSearchDatetimeStart.Size = new System.Drawing.Size(200, 22);
            this.dateTimePickerSearchDatetimeStart.TabIndex = 10;
            // 
            // dataGridViewSearch
            // 
            this.dataGridViewSearch.AllowUserToAddRows = false;
            this.dataGridViewSearch.AllowUserToDeleteRows = false;
            this.dataGridViewSearch.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewSearch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.dataGridViewSearch.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewSearch.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridViewSearch.MultiSelect = false;
            this.dataGridViewSearch.Name = "dataGridViewSearch";
            this.dataGridViewSearch.ReadOnly = true;
            this.dataGridViewSearch.Size = new System.Drawing.Size(982, 171);
            this.dataGridViewSearch.TabIndex = 2;
            // 
            // checkBoxDateSearch
            // 
            this.checkBoxDateSearch.AutoSize = true;
            this.checkBoxDateSearch.Location = new System.Drawing.Point(133, 22);
            this.checkBoxDateSearch.Name = "checkBoxDateSearch";
            this.checkBoxDateSearch.Size = new System.Drawing.Size(200, 21);
            this.checkBoxDateSearch.TabIndex = 22;
            this.checkBoxDateSearch.Text = "Использовать для поиска";
            this.checkBoxDateSearch.UseVisualStyleBackColor = true;
            // 
            // checkBoxStudetnSearch
            // 
            this.checkBoxStudetnSearch.AutoSize = true;
            this.checkBoxStudetnSearch.Location = new System.Drawing.Point(515, 23);
            this.checkBoxStudetnSearch.Name = "checkBoxStudetnSearch";
            this.checkBoxStudetnSearch.Size = new System.Drawing.Size(200, 21);
            this.checkBoxStudetnSearch.TabIndex = 23;
            this.checkBoxStudetnSearch.Text = "Использовать для поиска";
            this.checkBoxStudetnSearch.UseVisualStyleBackColor = true;
            // 
            // checkBoxSubjectSearch
            // 
            this.checkBoxSubjectSearch.AutoSize = true;
            this.checkBoxSubjectSearch.Location = new System.Drawing.Point(776, 23);
            this.checkBoxSubjectSearch.Name = "checkBoxSubjectSearch";
            this.checkBoxSubjectSearch.Size = new System.Drawing.Size(200, 21);
            this.checkBoxSubjectSearch.TabIndex = 24;
            this.checkBoxSubjectSearch.Text = "Использовать для поиска";
            this.checkBoxSubjectSearch.UseVisualStyleBackColor = true;
            // 
            // buttonSearchReset
            // 
            this.buttonSearchReset.Location = new System.Drawing.Point(507, 164);
            this.buttonSearchReset.Name = "buttonSearchReset";
            this.buttonSearchReset.Size = new System.Drawing.Size(185, 32);
            this.buttonSearchReset.TabIndex = 25;
            this.buttonSearchReset.Text = "Сбросить поиск";
            this.buttonSearchReset.UseVisualStyleBackColor = true;
            this.buttonSearchReset.Click += new System.EventHandler(this.buttonSearchReset_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(996, 440);
            this.Controls.Add(this.tabControl1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormMain";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStudents)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMarkGrade)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMarks)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSearch)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        public System.Windows.Forms.DataGridView dataGridViewStudents;
        public System.Windows.Forms.DataGridView dataGridViewMarks;
        private System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.TextBox textBoxId;
        public System.Windows.Forms.Button buttonStudentDelete;
        public System.Windows.Forms.Button buttonStudentUpdate;
        public System.Windows.Forms.Button buttonStudentAdd;
        public System.Windows.Forms.NumericUpDown numericUpDownAge;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox textBoxClass;
        public System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonMarkDelete;
        private System.Windows.Forms.Button buttonMarkAdd;
        public System.Windows.Forms.TextBox textBoxMarksId;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.ComboBox comboBoxMarksSubjects;
        public System.Windows.Forms.NumericUpDown numericUpDownMarkGrade;
        public System.Windows.Forms.ComboBox comboBoxMarkStudent;
        public System.Windows.Forms.DateTimePicker dateTimePickerMarksDatetime;
        public System.Windows.Forms.Button buttonMarkUpdate;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox3;
        public System.Windows.Forms.DataGridView dataGridViewSearch;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.ComboBox comboBoxSearchSubject;
        public System.Windows.Forms.ComboBox comboBoxSearchStudent;
        public System.Windows.Forms.DateTimePicker dateTimePickerSearchDatetimeStart;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        public System.Windows.Forms.DateTimePicker dateTimePickerSearchDatetimeFinish;
        public System.Windows.Forms.Button buttonMarksSearch;
        private System.Windows.Forms.Button buttonSearchReset;
        public System.Windows.Forms.CheckBox checkBoxSubjectSearch;
        public System.Windows.Forms.CheckBox checkBoxStudetnSearch;
        public System.Windows.Forms.CheckBox checkBoxDateSearch;
    }
}

